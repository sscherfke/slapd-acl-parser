SLAPD ACL Parser
================

[![pipeline status](https://gitlab.com/sscherfke/slapd-acl-parser/badges/master/pipeline.svg)](https://gitlab.com/sscherfke/slapd-acl-parser/commits/master)
[![coverage report](https://gitlab.com/sscherfke/slapd-acl-parser/badges/master/coverage.svg)](https://gitlab.com/sscherfke/slapd-acl-parser/commits/master)

A parser for SLAPD access control lists.

The current implementation parses most of the ACL grammar.  The remaining parts
were not needed yet.


## Installation and usage

```console
$ pip istall -e .[tests]
$ pytest
```

import os
import subprocess

from setuptools import setup, find_packages


setup(
    name='slapd-acl-parser',
    version='0.1',
    description='A parser for SLAPD access control lists',
    url='https://gitlab.com/sscherfke/slapd-acl-parser',
    install_requires=[
        'attrs>=17.4',
        'click~=7.0',
        'gssapi~=1.4;platform_system!="Windows"',
        'lark-parser~=0.7',
        'ldap3~=2.5',
    ],
    extras_require={
        'tests': ['pytest', 'pytest-cov'],
    },
    python_requires='>=3.7',
    packages=find_packages(where='src'),
    package_dir={'': 'src'},
    include_package_data=True,
    entry_points={
        'console_scripts': [
        ],
    },
    classifiers=[
        'Private :: Do Not Upload',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3 :: Only',
    ],
)

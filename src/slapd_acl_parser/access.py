# pylint: disable=no-self-use
"""
Functions to parse LDAP ACLs in YAML format (from our ansible) and check if
a DN can edit another DN.


"""
import enum
import functools
import re

import attr
import lark
import ldap3


__all__ = [
    'Level',
    'Directive', 'Rule',
    'check', 'parse_acl',
]


DN_MATCH_FUNCS = {}
GROUP_MEMBER_CACHE = {}
LEVEL_PRIVILEGES = {
    'none': set('0'),
    'disclose': set('d'),
    'auth': set('dx'),
    'compare': set('cdx'),
    'search': set('scdx'),
    'read': set('rscdx'),
    'write': set('wrcdx'),
    'manage': set('mwrscdx'),
}


class Level(enum.Enum):
    """Requested access level"""
    read = 'r'
    write = 'w'
    manage = 'm'


@attr.s()
class Directive:
    match_style = attr.ib()
    base_dn = attr.ib()
    attrs = attr.ib()
    rules = attr.ib()


@attr.s()
class Rule:
    match_style = attr.ib()
    match_value = attr.ib()
    access = attr.ib()
    control = attr.ib()


class AclTransformer(lark.Transformer):
    # See:
    # - https://www.openldap.org/doc/admin24/access-control.html
    # - https://linux.die.net/man/5/slapd.access
    grammar = """
    start: _NL* directive*
    directive: "access to" _WS what _NL rules
    what: dn | attrs | (dn _WS attrs)
    rules: rule+
    rule: _WS "by" _WS who [_WS access] [_WS control] _NL

    dn: match_style "=" ESCAPED_STRING
    attrs: "attrs=" WORD ("," WORD)*

    who: WHO_CONST | dn | group
    //group: "group" ["/" WORD ["/" WORD]]["." /regex|exact/] "=" ESCAPED_STRING
    group: "group/groupOfUniqueNames/uniqueMember" "=" ESCAPED_STRING
    access: LEVEL | PRIV
    control: CONTROL

    match_style: "dn" ["." MATCH_STYLE]
    MATCH_STYLE: /regex|exact|base|one|subtree|children/
    WHO_CONST: /\\*|anonymous|self|users/
    LEVEL: /none|disclose|auth|compare|search|read|write|manage/
    PRIV: /[=\\+-][mwrscxd0]+/
    CONTROL: /stop|continue|break/

    COMMENT: /#[^\\n]*/
    _NL: ( _WS? COMMENT? NEWLINE )+
    %ignore COMMENT

    %import common.WORD
    %import common.ESCAPED_STRING
    %import common.WS_INLINE -> _WS
    %import common.NEWLINE

    """

    def start(self, args):
        # Just convert the Tree item to a normal list of directives
        # args.append(Directive('*', '', None, [Rule('const', '*', 'none', 'stop')))
        return args

    @lark.v_args(inline=True)
    def directive(self, what, rules):
        return Directive(**what, rules=rules)

    def what(self, args):
        """Return a tuple *(dn_match_func, attr_match_func)* for *args*.

        *args* can be:

        - ``[dn]``
        - ``[attrs]``
        - ``[dn, attrs]``

        """
        data = {
            'match_style': '*',
            'base_dn': '',
            'attrs': None,
        }
        for arg in args:
            data.update(arg)

        return data

    def rules(self, args):
        # Just convert the Tree item to an normal list of rules
        # args.append(Rule('const', '*', 'none', 'stop'))
        return args

    def rule(self, args):
        kwargs = {
            'access': 'none',
            'control': 'stop',
        }
        for arg in args:
            kwargs.update(arg)
        return Rule(**kwargs)

    @lark.v_args(inline=True)
    def dn(self, match_style, dn_string):
        try:
            match_style = match_style.children[0].value
        except IndexError:
            match_style = 'base'
        return {
            'match_style': match_style,
            'base_dn': dn_string[1:-1],  # strip enclosing '"'
        }

    def attrs(self, tokens):
        names = {t.value for t in tokens}
        return {'attrs': names}

    @lark.v_args(inline=True)
    def who(self, who):
        if isinstance(who, dict):
            # Return value from :meth:`dn()`
            value = who
            value['match_value'] = value.pop('base_dn')  # Rename key
        elif isinstance(who, lark.Tree):
            # A "group" Tree
            assert who.data == 'group'
            value = {'match_style': 'group', 'match_value': who.children[0].value[1:-1]}
        else:
            # A "const" token
            assert who.type == 'WHO_CONST'
            value = {'match_style': 'const', 'match_value': who.value}

        return value

    @lark.v_args(inline=True)
    def access(self, token):
        return {'access': token.value}

    @lark.v_args(inline=True)
    def control(self, token):
        return {'control': token.value}


def parse_acl(fp):
    """Parse the ACL from *fp* (a file-like iterable) and return a list of
    access directives (:class:`AccessDirective`).

    """
    parser = lark.Lark(
        AclTransformer.grammar,
        parser='lalr',
        transformer=AclTransformer(),
    )
    acl = parser.parse(fp.read())
    return acl


def check(conn, acl, level, who, what, attrib=None):
    """Check if *acl* provides *level* access for *who* at dn *what* and
    attribute *attr*.  Use *conn* and *group_cache*...

    """
    # has_access = False
    privileges = LEVEL_PRIVILEGES['none']
    for directive in acl:
        if not directive_applies(directive, what, attrib):
            continue

        do_stop = True
        for rule in directive.rules:
            if not rule_applies(rule, who, what, conn):
                continue

            privileges = update_privileges(privileges, rule)

            if rule.control == 'continue':
                continue

            if rule.control == 'break':
                do_stop = False  # Evaluate another directive

            # Control is break or stop -> stop rule evaluation
            break

        if do_stop:
            break

    return level.value in privileges


def directive_applies(directive, what, attrib):
    directive_matches = DN_MATCH_FUNCS[directive.match_style]
    if not directive_matches(directive.base_dn, what):
        return False

    attr_only_directive = not directive.base_dn and directive.attrs
    if not attrib and attr_only_directive:
        # Directive does not apply if we check against an entiry w/o attrib but
        # the directive only adds privileges for attributes
        return False

    if directive.attrs and attrib not in directive.attrs:
        # If the directive specifies attrs, "attrib" must be in it
        return False

    return True


def rule_applies(rule, who, what, conn):
    """Return a ``True`` if *rule* to *who*.

    *what* is the accessed DN and *conn* the LDAP connection.

    """
    try:
        match_func = DN_MATCH_FUNCS[rule.match_style]
        applies = functools.partial(match_func, rule.match_value)
    except KeyError:
        if rule.match_style == 'const':
            funcs = {
                'anonymous': lambda who: False,
                'self': lambda who: who == what,
                'users': lambda who: True,
                '*': lambda who: True,
            }
            applies = funcs[rule.match_value]

        else:
            assert rule.match_style == 'group'
            applies = lambda who: who in get_group_members(conn, rule.match_value)

    return applies(who)


def update_privileges(privileges, rule):
    try:
        privileges = LEVEL_PRIVILEGES[rule.access]
    except KeyError:
        op, rule_privs = rule.access[0], set(rule.access[1:])
        if op == '+':
            privileges |= rule_privs
        elif op == '-':
            privileges -= rule_privs
        else:
            assert op == '=', op
            privileges = rule_privs
    return privileges


def get_group_members(conn, dn):
    """Return the list of group members of *dn*."""
    if dn in GROUP_MEMBER_CACHE:
        return GROUP_MEMBER_CACHE[dn]

    success = conn.search(
        dn,
        f'(objectclass=groupOfUniqueNames)',
        search_scope=ldap3.BASE,
        attributes=['uniqueMember'],
    )
    if success:
        assert len(conn.entries) == 1
        result = conn.entries[0].uniqueMember.values
        GROUP_MEMBER_CACHE[dn] = result
        return result

    return []


def match(*names):
    def wrapper(f):
        for name in names:
            DN_MATCH_FUNCS[name] = f
        return f

    return wrapper


@match('*')
def dn_match_all(_pattern, _dn):
    """Always return ``True``."""
    return True


@match('regex')
def dn_match_regex(pattern, dn):
    """Return ``True`` if *dn* matches *pattern*."""
    return re.search(pattern, dn)


@match('base', 'exact')
def dn_match_base(base, dn):
    """Return ``True`` if *dn* equals *base*."""
    return base == dn


@match('one')
def dn_match_one(base, dn):
    """Return ``True`` if *base* is the direct parent of *dn*."""
    base = base.split(',')
    dn = dn.split(',')
    return len(dn) == len(base) + 1 and dn[1:] == base


@match('subtree')
def dn_match_subtree(base, dn):
    """Return ``True`` if *dn* is *base* or one of its children."""
    base = base.split(',')
    dn = dn.split(',')
    base_len = len(base)
    return len(dn) >= base_len and dn[-base_len:] == base


@match('children')
def dn_match_children(base, dn):
    """Return ``True`` if *dn* is one of *base*'s children (but not *base*
    itself).

    """
    base = base.split(',')
    dn = dn.split(',')
    base_len = len(base)
    return len(dn) > base_len and dn[-base_len:] == base

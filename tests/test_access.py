# pylint: disable=bad-whitespace,redefined-outer-name,line-too-long
import pathlib

import ldap3
import pytest

from slapd_acl_parser.access import Directive, Level, Rule, check, parse_acl


DATA_DIR = pathlib.Path(__file__).parent / 'data'
TEST_USER = 'urmel'
TEST_PWD = 'Elves=agent-Dash'
STEVE = 'uid=steve,ou=people,ou=urmel,ou=testing,dc=example,dc=com'
JON = 'uid=jon,ou=people,ou=urmel,ou=testing,dc=example,dc=com'
MO = 'uid=mo,ou=people,ou=urmel,ou=testing,dc=example,dc=com'


@pytest.fixture
def ldap_conn():
    fake_server = ldap3.Server.from_definition(
        'test_server',
        str(DATA_DIR / 'server_info.json'),
        str(DATA_DIR / 'server_schema.json'),
    )
    fake_connection = ldap3.Connection(
        fake_server,
        user='cn=pytest,dc=example,dc=com',
        password='pytest',
        client_strategy=ldap3.MOCK_SYNC,
    )

    # Add a fake user for Simple binding
    fake_connection.strategy.add_entry(
        'cn=pytest,dc=example,dc=com',
        {
            'userPassword': 'pytest',
            'sn': 'pytest',
        },
    )

    # Populate the DIT of the fake server
    fake_connection.strategy.entries_from_json(
        str(DATA_DIR / 'server_entries.json'))

    fake_connection.bind()

    fake_connection.check_acl = True
    fake_connection.can_edit = lambda what: True
    fake_connection.can_manage = lambda what: True

    return fake_connection


@pytest.fixture
def acl():
    return parse_acl(open(DATA_DIR / 'acl.yaml'))


def test_parse_acl(acl):
    expected = [
        Directive(
            match_style='subtree',
            base_dn='ou=people,ou=urmel,ou=testing,dc=example,dc=com',
            attrs=None,
            rules=[
                Rule(match_style='const', match_value='self', access='write', control='stop'),
                Rule(match_style='const', match_value='*', access='none', control='break'),
            ],
        ),
        Directive(
            match_style='subtree',
            base_dn='ou=testing,dc=example,dc=com',
            attrs=None,
            rules=[
                Rule(match_style='base', match_value='uid=admin,ou=people,dc=example,dc=com', access='manage', control='stop'),
            ],
        ),
        Directive(
            match_style='subtree', base_dn='ou=urmel,ou=testing,dc=example,dc=com',
            attrs=None,
            rules=[
                Rule(match_style='base', match_value='uid=jon,ou=people,ou=urmel,ou=testing,dc=example,dc=com', access='manage', control='stop'),
                Rule(match_style='group', match_value='cn=devs,ou=roles,ou=urmel,ou=testing,dc=example,dc=com', access='manage', control='stop'),
                Rule(match_style='const', match_value='anonymous', access='auth', control='stop'),
                Rule(match_style='const', match_value='self', access='=wdx', control='stop'),
                Rule(match_style='const', match_value='*', access='none', control='stop'),
            ],
        ),
        Directive(
            match_style='regex',
            base_dn='ou=spam([^,]+),ou=roles,ou=external,dc=example,dc=com$',
            attrs=None,
            rules=[
                Rule(match_style='group', match_value='cn=roles-spam,ou=groups,dc=example,dc=com', access='manage', control='stop'),
            ],
        ),
        Directive(
            match_style='subtree',
            base_dn='ou=contacts,dc=example,dc=com',
            attrs={'userPassword'},
            rules=[
                Rule(match_style='const', match_value='*', access='none', control='stop'),
            ],
        ),
        Directive(
            match_style='*',
            base_dn='',
            attrs={'userPassword', 'telephoneNumber'},
            rules=[
                Rule(match_style='base', match_value='cn=manager,dc=example,dc=com', access='write', control='stop'),
                Rule(match_style='base', match_value='gidNumber=0+uidNumber=0,cn=peercred,cn=external,cn=auth', access='write', control='stop'),
                Rule(match_style='base', match_value='uid=admin,ou=people,dc=example,dc=com', access='write', control='stop'),
                Rule(match_style='const', match_value='self', access='write', control='stop'),
                Rule(match_style='const', match_value='*', access='read', control='stop'),
            ],
        ),
    ]
    for directive, e in zip (acl, expected):
        assert directive == e


@pytest.mark.parametrize(('what, attrib, expected'), [
    # default, exact and base should all be the same
    (                 'ou=default,dc=example,dc=com', None, True),
    (        'ou=child,ou=default,dc=example,dc=com', None, False),
    (                   'ou=exact,dc=example,dc=com', None, True),
    (          'ou=child,ou=exact,dc=example,dc=com', None, False),
    (                    'ou=base,dc=example,dc=com', None, True),
    (           'ou=child,ou=base,dc=example,dc=com', None, False),
    # Only match direct child
    (                     'ou=one,dc=example,dc=com', None, False),
    (            'ou=child,ou=one,dc=example,dc=com', None, True),
    (     'ou=sub,ou=child,ou=one,dc=example,dc=com', None, False),
    # Only match dildren (not parent)
    (                'ou=children,dc=example,dc=com', None, False),
    (       'ou=child,ou=children,dc=example,dc=com', None, True),
    ('ou=sub,ou=child,ou=children,dc=example,dc=com', None, True),
    # Match parent and all children
    (                 'ou=subtree,dc=example,dc=com', None, True),
    (        'ou=child,ou=subtree,dc=example,dc=com', None, True),
    ( 'ou=sub,ou=child,ou=subtree,dc=example,dc=com', None, True),
    # Match everything with ou=spam.*
    (                    'ou=spam,dc=example,dc=com', None, True),
    (                'ou=spameggs,dc=example,dc=com', None, True),
    (       'ou=child,ou=spameggs,dc=example,dc=com', None, True),
    # Checking attrib acces when rule has not attrib restrictions
    (                    'ou=base,dc=example,dc=com', 'a',  True),
    (                 'ou=subtree,dc=example,dc=com', 'b',  True),
    # a,b directly allowed, c,d via another rule:
    (                   'ou=attrs,dc=example,dc=com', None, False),
    (                   'ou=attrs,dc=example,dc=com', 'a',  True),
    (                   'ou=attrs,dc=example,dc=com', 'c',  True),
    (                   'ou=attrs,dc=example,dc=com', 'e',  False),
    # The c,d rule should also apply here:
    (                 'ou=unknown,dc=example,dc=com', None, False),
    (                 'ou=unknown,dc=example,dc=com', 'd',  True),
])
def test_acl_what(ldap_conn, what, attrib, expected):
    acl = parse_acl(open(DATA_DIR / 'acl-test-what.yaml'))
    who = 'uid=steve,ou=people,ou=urmel,ou=testing,dc=example,dc=com'
    assert check(ldap_conn, acl, Level.write, who, what, attrib) is expected


@pytest.mark.parametrize(('what, who, expected'), [
    ('ou=dn,dc=example,dc=com', STEVE, True),
    ('ou=dn,dc=example,dc=com', JON, False),
    ('ou=group,dc=example,dc=com', STEVE, True),
    ('ou=group,dc=example,dc=com', JON, False),
    ('cn=steve,ou=self,dc=example,dc=com', 'cn=steve,ou=self,dc=example,dc=com', True),
    ('ou=anonymous,dc=example,dc=com', STEVE, False),
    ('ou=anonymous,dc=example,dc=com', JON, False),
    ('ou=anonymous,dc=example,dc=com', MO, False),
    ('ou=users,dc=example,dc=com', STEVE, True),
    ('ou=users,dc=example,dc=com', JON, True),
    ('ou=users,dc=example,dc=com', MO, True),
    ('ou=star,dc=example,dc=com', STEVE, True),
    ('ou=star,dc=example,dc=com', JON, True),
    ('ou=star,dc=example,dc=com', MO, True),
])
def test_acl_who(ldap_conn, what, who, expected):
    acl = parse_acl(open(DATA_DIR / 'acl-test-who.yaml'))
    assert check(ldap_conn, acl, Level.write, who, what) is expected


@pytest.mark.parametrize('what, who, level, expected', [
    ('test,people,urmel,example', 'steve', Level.manage, False),
    ('test,people,urmel,example', 'steve', Level.write, True),
    ('test,people,urmel,example', 'jon', Level.manage, True),
    ('test,people,urmel,example', 'mo', Level.manage, True),  # From the second directive
    ('test,bats,example', 'steve', Level.write, False),
    ('test,bats,example', 'jon', Level.manage, True),
    ('test,bats,example', 'mo', Level.manage, False),
    ('test,bats,example', 'mo', Level.write, True),
])
def test_acl_control(ldap_conn, what, who, level, expected):
    acl = parse_acl(open(DATA_DIR / 'acl-test-control.yaml'))
    assert check(ldap_conn, acl, level, who, what) is expected
